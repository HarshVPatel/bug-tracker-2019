﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace bugtracker.Models
{
    public class DashboardViewModel
    {
        public int Tickets { get; set; }

        public int Projects { get; set; }

        public int Opened { get; set; }

        public int Resolved { get; set; }

        public int Closed { get; set; }

    }
}