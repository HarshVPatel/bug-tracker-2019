﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace bugtracker.Models
{
    public class AssignTicketsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public SelectList ListofDevelopers { get; set; }
        public string SelectedDeveloperId { get; set; }
    }
}