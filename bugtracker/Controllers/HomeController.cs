﻿using bugtracker.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace bugtracker.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private ApplicationDbContext Context;

        public HomeController()
        {
            Context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            var model = new DashboardViewModel();
            if(User.IsInRole("Admin")|| User.IsInRole("ProjectManager"))
            {
                var ticket = Context.Tickets.Count();
                var projects = Context.Projects.Count();
                var open = Context.Tickets.Where(p => p.TicketStatus.Name == "Open").Count();
                var resolved = Context.Tickets.Where(p => p.TicketStatus.Name == "Resolved").Count();
                var closed = Context.Tickets.Where(p => p.TicketStatus.Name == "Rejected").Count();
                model.Tickets = ticket;
                model.Projects = projects;
                model.Opened = open;
                model.Resolved = resolved;
                model.Closed = closed;
            }
           
            if (User.IsInRole("Submitter"))
            {
                var userId = User.Identity.GetUserId();

                var ticketsSubmitter = Context.Tickets.Where(p => p.CreatorId == userId).Include(t => t.Assignee).Include(t => t.Creator).Include(t => t.Project).Include(t => t.TicketPriority).Include(t => t.TicketStatus).Include(t => t.TicketType);
                var ticket = ticketsSubmitter.Count();
                var projects = Context.Projects.Where(p => p.Users.Any(t => t.Id == userId)).Count();
                var open = ticketsSubmitter.Where(p => p.TicketStatus.Name == "Open").Count();
                var resolved = ticketsSubmitter.Where(p => p.TicketStatus.Name == "Resolved").Count();
                var closed = ticketsSubmitter.Where(p => p.TicketStatus.Name == "Rejected").Count();
                model.Tickets = ticket;
                model.Projects = projects;
                model.Opened = open;
                model.Resolved = resolved;
                model.Closed = closed;

            }
            if (User.IsInRole("Developer"))
            {
                var userId = User.Identity.GetUserId();
                var ticketsDeveloper = Context.Tickets.Where(p => p.AssigneeId == userId).Include(t => t.Assignee).Include(t => t.Creator).Include(t => t.Project).Include(t => t.TicketPriority).Include(t => t.TicketStatus).Include(t => t.TicketType);
                var projects = Context.Projects.Where(p => p.Users.Any(t => t.Id == userId)).Count();
                var ticket = ticketsDeveloper.Count();
                var open = ticketsDeveloper.Where(p => p.TicketStatus.Name == "Open").Count();
                var resolved = ticketsDeveloper.Where(p => p.TicketStatus.Name == "Resolved").Count();
                var closed = ticketsDeveloper.Where(p => p.TicketStatus.Name == "Rejected").Count();
                model.Tickets = ticket;
                model.Projects = projects;
                model.Opened = open;
                model.Resolved = resolved;
                model.Closed = closed;
            }
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}