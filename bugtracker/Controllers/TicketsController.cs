﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using bugtracker.Helpers;
using bugtracker.Models;
using Microsoft.AspNet.Identity;

namespace bugtracker.Controllers
{
    [Authorize]

    public class TicketsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Tickets
        public ActionResult Index()
        {
            var tickets = db.Tickets.
                Include(t => t.Assignee).
                Include(t => t.Creator).
                Include(t => t.Project).
                Include(t => t.TicketPriority).
                Include(t => t.TicketStatus).
                Include(t => t.TicketType);
            return View(tickets.ToList());
        }


        public ActionResult AssignTicketToDeveloper(int ticketId)
        {
            var model = new AssignTicketsViewModel();
            var ticket = db.Tickets.FirstOrDefault(p => p.Id == ticketId);
            var userRoleHelper = new UserRoleHelper();
            var users = userRoleHelper.UsersInRole("Developer");
            model.Id = ticketId;
            model.ListofDevelopers = new SelectList(users, "Id", "Name");
            return View(model);
        }

        [HttpPost]
        public ActionResult AssignTicketToDeveloper(AssignTicketsViewModel model)
        {
            var ticket = db.Tickets.FirstOrDefault(p => p.Id == model.Id);
            ticket.AssigneeId = model.SelectedDeveloperId;
            var user = db.Users.FirstOrDefault(p => p.Id == model.SelectedDeveloperId);
            ticket.Assignee = user;
            if (ticket.AssigneeId != null)
            {
                var personalEmailService = new PersonalEmailService();
                var mailMessage = new MailMessage(
                WebConfigurationManager.AppSettings["emailto"], ticket.Assignee.Email);
                mailMessage.Subject = "New Ticket";
                mailMessage.Body = "Boom! You have been assigned a new ticket";
                mailMessage.IsBodyHtml = true;
                personalEmailService.Send(mailMessage);
            }
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult SubmitterTickets()
        {
            var userId = User.Identity.GetUserId();
            var ticketsSubmitter = db.Tickets.Where(p => p.CreatorId == userId).Include(t => t.Assignee).Include(t => t.Creator).Include(t => t.Project).Include(t => t.TicketPriority).Include(t => t.TicketStatus).Include(t => t.TicketType);
            return View("Index", ticketsSubmitter.ToList());
        }

        public ActionResult DeveloperTickets()
        {
            var userId = User.Identity.GetUserId();
            var ticketsDeveloper = db.Tickets.Where(ticket => ticket.AssigneeId == userId).Include(t => t.Assignee).Include(t => t.Creator).Include(t => t.Project).Include(t => t.TicketPriority).Include(t => t.TicketStatus).Include(t => t.TicketType);
            return View("Index", ticketsDeveloper.ToList());
        }

        public ActionResult TicketsForMyProjects()
        {
            string userId = User.Identity.GetUserId();
            var developerproject = db.Users.Where(p => p.Id == userId).FirstOrDefault();
            var projectsIds = developerproject.Projects.Select(p => p.Id).ToList();
            var Ticket = db.Tickets.Where(p => projectsIds.Contains(p.ProjectId)).ToList();
            return View("Index", Ticket);
        }

        // GET: Tickets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tickets tickets = db.Tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }

            return View(tickets);
        }

        // GET: Tickets/Create
        [Authorize(Roles = "Submitter")]
        public ActionResult Create()
        {

            var userId = User.Identity.GetUserId();

            var projects = db.Projects
                .Where(p => p.Users.Any(m => m.Id == userId))
                .Select(p => new { p.Name, p.Id }).ToList();

            ViewBag.CreatorId = new SelectList(db.Users, "Id", "Name");
            ViewBag.ProjectId = new SelectList(projects, "Id", "Name");
            ViewBag.TicketPriorityId = new SelectList(db.TicketPriorities, "Id", "Name");
            ViewBag.TicketStatusId = new SelectList(db.TicketStatuses, "Id", "Name");
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name");
            return View();
        }

        // POST: Tickets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Submitter")]
        public ActionResult Create([Bind(Include = "Id,Name,Description,Updated,TicketTypeId,TicketPriorityId,AssigneeId,ProjectId")] Tickets tickets)
        {
            if (ModelState.IsValid)
            {
                tickets.CreatorId = User.Identity.GetUserId();
                tickets.TicketStatusId = 1;
                db.Tickets.Add(tickets);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CreatorId = new SelectList(db.Users, "Id", "Name", tickets.CreatorId);
            ViewBag.TicketPriorityId = new SelectList(db.TicketPriorities, "Id", "Name", tickets.TicketPriorityId);
            ViewBag.TicketStatusId = new SelectList(db.TicketStatuses, "Id", "Name", tickets.TicketStatusId);
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name", tickets.TicketTypeId);
            ViewBag.ProjectId = new SelectList(db.Projects, "Id", "Name", tickets.ProjectId);
            return View(tickets);
        }

        // GET: Tickets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tickets tickets = db.Tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            var userId = User.Identity.GetUserId();

            var projects = db.Projects
                .Where(p => p.Users.Any(m => m.Id == userId))
                .Select(p => new { p.Name, p.Id }).ToList();

            ViewBag.AssigneeId = new SelectList(db.Users, "Id", "DisplayName", tickets.AssigneeId);
            ViewBag.CreatorId = new SelectList(db.Users, "Id", "DisplayName", tickets.CreatorId);
            ViewBag.ProjectId = new SelectList(projects, "Id", "Name", tickets.ProjectId);
            ViewBag.TicketPriorityId = new SelectList(db.TicketPriorities, "Id", "Name", tickets.TicketPriorityId);
            ViewBag.TicketStatusId = new SelectList(db.TicketStatuses, "Id", "Name", tickets.TicketStatusId);
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name", tickets.TicketTypeId);
            return View(tickets);
        }

        // POST: Tickets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,TicketTypeId,TicketPriorityId,TicketStatusId,AssigneeId,ProjectId")] Tickets tickets)
        {
            if (ModelState.IsValid)
            {
                var datechanged = DateTimeOffset.Now;
                var changes = new List<TicketHistory>();

                var ticket = db.Tickets.FirstOrDefault(p => p.Id == tickets.Id);
                ticket.Name = tickets.Name;
                ticket.ProjectId = tickets.ProjectId;
                ticket.Description = tickets.Description;
                ticket.Updated = DateTime.Now;
                if (User.IsInRole("Admin") || User.IsInRole("Project Manager"))
                {
                    ticket.TicketStatusId = tickets.TicketStatusId;
                }
                ticket.TicketPriorityId = tickets.TicketPriorityId;
                ticket.TicketTypeId = tickets.TicketTypeId;

                var originalValues = db.Entry(ticket).OriginalValues;
                var CurrentValues = db.Entry(ticket).CurrentValues;

                foreach (var property in originalValues.PropertyNames)
                {
                    var originalValue = originalValues[property]?.ToString();
                    var currentValue = CurrentValues[property]?.ToString();

                    if (originalValue != currentValue)
                    {
                        var history = new TicketHistory();
                        history.Changed = datechanged;
                        history.NewValue = GetValueFromKey(property, currentValue);
                        history.OldValue = GetValueFromKey(property, originalValue);
                        history.Property = property;
                        history.TicketId = ticket.Id;
                        history.UserId = User.Identity.GetUserId();
                        changes.Add(history);


                    }
                }
                db.TicketHistories.AddRange(changes);
                if (tickets.AssigneeId != null)
                {
                    var personalEmailService = new PersonalEmailService();
                    var mailMessage = new MailMessage(
                    WebConfigurationManager.AppSettings["emailto"], tickets.Assignee.Email);
                    mailMessage.Subject = "ticket edited";
                    mailMessage.Body = "There are some changes made on your ticket";
                    mailMessage.IsBodyHtml = true;
                    personalEmailService.Send(mailMessage);
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CreatorId = new SelectList(db.Users, "Id", "Name", tickets.CreatorId);
            ViewBag.TicketPriorityId = new SelectList(db.TicketPriorities, "Id", "Name", tickets.TicketPriorityId);
            ViewBag.TicketStatusId = new SelectList(db.TicketStatuses, "Id", "Name", tickets.TicketStatusId);
            ViewBag.TicketTypeId = new SelectList(db.TicketTypes, "Id", "Name", tickets.TicketTypeId);
            ViewBag.ProjectId = new SelectList(db.Projects, "Id", "Name", tickets.ProjectId);
            return View(tickets);
        }

        private string GetValueFromKey(string property, string currentValue)
        {
            return currentValue;
        }


        // GET: Tickets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tickets tickets = db.Tickets.Find(id);
            if (tickets == null)
            {
                return HttpNotFound();
            }
            return View(tickets);
        }

        // POST: Tickets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tickets tickets = db.Tickets.Find(id);
            db.Tickets.Remove(tickets);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult CreateComment(int id, string body)
        {
            var tickets = db.Tickets.Where(p => p.Id == id).FirstOrDefault();
            var userId = User.Identity.GetUserId();

            if (tickets == null)
            {
                return HttpNotFound();
            }

            if (string.IsNullOrWhiteSpace(body))
            {
                ViewBag.ErrorMessage = "Comment is required";
                return View("Details", tickets);
            }

            if ((User.IsInRole("Admin")) || (User.IsInRole("Project Manager") || (User.IsInRole("Submitter") && tickets.CreatorId == userId) || (User.IsInRole("Developer") && tickets.Assignee.Id == userId)))
            {
                var comment = new TicketComment();
                comment.UserId = User.Identity.GetUserId();
                comment.TicketId = tickets.Id;
                comment.Created = DateTime.Now;
                comment.Comment = body;
                db.TicketComments.Add(comment);
                if (tickets.AssigneeId != null)
                {
                    var personalEmailService = new PersonalEmailService();
                    var mailMessage = new MailMessage(
                    WebConfigurationManager.AppSettings["emailto"], tickets.Assignee.Email);
                    mailMessage.Subject = "New comment";
                    mailMessage.Body = "There is a new comment in the ticket";
                    mailMessage.IsBodyHtml = true;
                    personalEmailService.Send(mailMessage);
                }
                db.SaveChanges();
            }
            else if (User.Identity.IsAuthenticated)
            {
                ViewBag.ErrorMessage = "Sorry! you are not allowed to comment.";
                return View("Details", tickets);
            }
            return RedirectToAction("Details", new { id });
        }

        [HttpGet]
        public ActionResult EditComment(int? id)
        {

            if (!id.HasValue)
            {
                return RedirectToAction(nameof(TicketsController.Index));
            }

            var comment = db.TicketComments.FirstOrDefault(p => p.Id == id.Value);

            if (comment == null)
            {
                return RedirectToAction(nameof(TicketsController.Index));
            }

            var model = new TicketComment();
            model.Id = comment.Id;
            model.Comment = comment.Comment;
            model.Created = comment.Created;
            model.Updated = comment.Updated;
            return View(model);
        }

        [HttpPost]
        public ActionResult EditComment(int? id, EditComment model)
        {
            if (!id.HasValue)
            {
                return RedirectToAction(nameof(TicketsController.Index));
            }

            if (!ModelState.IsValid)
            {
                return View();
            }

            var comment = db.TicketComments.FirstOrDefault(p => p.Id == id.Value);
            comment.Id = model.Id;
            comment.Comment = model.Comment;
            comment.Updated = DateTime.Now;

            db.SaveChanges();
            return RedirectToAction(nameof(TicketsController.Index));

        }

        public ActionResult DeleteComment(int? id)
        {

            if (!id.HasValue)
            {
                return RedirectToAction(nameof(TicketsController.Index));
            }

            var comment = db.TicketComments.FirstOrDefault(p => p.Id == id.Value);

            if (comment == null)
            {
                return RedirectToAction(nameof(TicketsController.Index));
            }

            db.TicketComments.Remove(comment);
            db.SaveChanges();

            return RedirectToAction(nameof(TicketsController.Index));
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAttachment(int ticketId, [Bind(Include = "Id,Description,TicketTypeId")] TicketAttachment ticketAttachment, HttpPostedFileBase image)
        {
            var tickets = db.Tickets.Where(p => p.Id == ticketId).FirstOrDefault();
            var userId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if (image == null)
                {
                    return HttpNotFound();
                }

                if (!ImageUploadValidator.IsWebFriendlyImage(image))
                {
                    ViewBag.ErrorMessage = "Please upload an image";
                }
                if ((User.IsInRole("Admin")) || (User.IsInRole("Project Manager") || (User.IsInRole("Submitter") && tickets.CreatorId == userId) || (User.IsInRole("Developer") && tickets.Assignee.Id == userId)))
                {
                    var fileName = Path.GetFileName(image.FileName);
                    image.SaveAs(Path.Combine(Server.MapPath("~/Uploads/"), fileName));
                    ticketAttachment.FilePath = "/Uploads/" + fileName;
                    ticketAttachment.UserId = User.Identity.GetUserId();
                    ticketAttachment.Created = DateTime.Now;
                    ticketAttachment.TicketId = ticketId;
                    db.TicketAttachments.Add(ticketAttachment);
                    if (tickets.AssigneeId != null)
                    {
                        var personalEmailService = new PersonalEmailService();
                        var mailMessage = new MailMessage(
                        WebConfigurationManager.AppSettings["emailto"], tickets.Assignee.Email);
                        mailMessage.Subject = "New Attachment";
                        mailMessage.Body = "There is a new attachment in the ticket";
                        mailMessage.IsBodyHtml = true;
                        personalEmailService.Send(mailMessage);
                    }
                    db.SaveChanges();
                }
                else if (User.Identity.IsAuthenticated)
                {
                    ViewBag.ErrorMessage = "Sorry! you are not allowed to attach document.";
                    return View("Details", tickets);
                }
                return RedirectToAction("Details", new { id = ticketId });
            }
            return View(ticketAttachment);
        }
        public ActionResult DeleteAttachment(int? id)
        {

            if (!id.HasValue)
            {
                return RedirectToAction(nameof(TicketsController.Index));
            }

            var attachment = db.TicketAttachments.FirstOrDefault(p => p.Id == id.Value);

            if (attachment == null)
            {
                return RedirectToAction(nameof(TicketsController.Index));
            }

            db.TicketAttachments.Remove(attachment);

            db.SaveChanges();

            return RedirectToAction(nameof(TicketsController.Index));
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
