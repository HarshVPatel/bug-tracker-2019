﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using bugtracker.Models;
using Microsoft.AspNet.Identity;

namespace bugtracker.Controllers
{
    [Authorize]
    public class ProjectsController : Controller
    {
        private ApplicationDbContext Context;

        public ProjectsController()
        {
            Context = new ApplicationDbContext();
        }

        public ActionResult Index()
        {

            var model = Context.Projects
                .Select(p => new ProjectIndexViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    AssignedUsers = p.Users.Count,
                    Tickets = 0,
                    Created = p.DateCreated,
                    Updated = p.DateUpdated
                }).ToList();


            return View(model);
        }

       
        
        public ActionResult IndexMyProjects()
        {
            var userId = User.Identity.GetUserId();

            var model = Context
                .Projects
                .Where(p => p.Users.Any(t => t.Id == userId))
                .Select(p => new ProjectIndexViewModel
                {
                    Id = p.Id,
                    Name = p.Name,
                    AssignedUsers = p.Users.Count,
                    Tickets = 0,
                    Created = p.DateCreated,
                    Updated = p.DateUpdated
                }).ToList();

            return View(model);
        }


        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateIndexViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var userId = User.Identity.GetUserId();

            var project = new Project();
            project.Id = model.Id;
            project.Name = model.Name;
            project.DateCreated = DateTime.Now;




            Context.Projects.Add(project);
            Context.SaveChanges();

            return RedirectToAction(nameof(ProjectsController.Index));
        }

        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!id.HasValue)
            {
                return RedirectToAction(nameof(ProjectsController.Index));
            }

            var project = Context.Projects.FirstOrDefault(p => p.Id == id.Value);

            if (project == null)
            {
                return RedirectToAction(nameof(ProjectsController.Index));
            }

            var model = new ProjectIndexViewModel();
            model.Id = project.Id;
            model.Name = project.Name;
            model.Created = project.DateCreated;
            model.Updated = project.DateUpdated;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(int? id, CreateIndexViewModel model)
        {
            if (!id.HasValue)
            {
                return RedirectToAction(nameof(ProjectsController.Index));
            }

            if (!ModelState.IsValid)
            {
                return View();
            }
            var project = Context.Projects.FirstOrDefault(p => p.Id == id.Value);

            project.Id = model.Id;
            project.Name = model.Name;
            project.DateUpdated = DateTime.Now;


            Context.SaveChanges();

            return RedirectToAction(nameof(ProjectsController.Index));

        }

        public ActionResult AssignUsers(int id)
        {
            var model = new AssignProjectsViewModel();
            model.Id = id;
            var project = Context.Projects.FirstOrDefault(p => p.Id == id);
            var users = Context.Users.ToList();
            var userIdsAssignedToProject = project.Users
                .Select(p => p.Id).ToList();
            model.UserList = new MultiSelectList(users, "Id", "Name", userIdsAssignedToProject);
            
            return View(model);


        }
        [HttpPost]
        public ActionResult AssignUsers(AssignProjectsViewModel model)
        {
            //STEP 1: Find the project
            var project = Context.Projects.FirstOrDefault(p => p.Id == model.Id);
            //STEP 2: Remove all assigned users from this project
            var assignedUsers = project.Users.ToList();
            foreach (var user in assignedUsers)
            {
                project.Users.Remove(user);
            }
            //STEP 3: Assign users to the project
            if (model.SelectedUsers != null)
            {
                foreach (var userId in model.SelectedUsers)
                {
                    var user = Context.Users.FirstOrDefault(p => p.Id == userId);
                    project.Users.Add(user);
                }
            }
           
            //STEP 4: Save changes to the database
            Context.SaveChanges();

            return RedirectToAction(nameof(ProjectsController.Index));
        }

        // GET: Projects/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Project project = Context.Projects.Find(id);
            if (project == null)
            {
                return HttpNotFound();
            }
            return View(project);
        }

        // POST: Projects/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Project project = Context.Projects.Find(id);
            Context.Projects.Remove(project);
            Context.SaveChanges();
            return RedirectToAction("Index");
        }
    }

}

