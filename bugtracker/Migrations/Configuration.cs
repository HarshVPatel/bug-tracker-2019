namespace bugtracker.Migrations
{
    using bugtracker.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<bugtracker.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "bugtracker.Models.ApplicationDbContext";
        }

        protected override void Seed(bugtracker.Models.ApplicationDbContext context)
        {
            var roleManager =
       new RoleManager<IdentityRole>(
           new RoleStore<IdentityRole>(context));

            var userManager =
                new UserManager<ApplicationUser>(
                        new UserStore<ApplicationUser>(context));

            if (!context.Roles.Any(p => p.Name == "Admin"))
            {
                roleManager.Create(new IdentityRole("Admin"));
            }

            if (!context.Roles.Any(p => p.Name == "Project Manager"))
            {
                roleManager.Create(new IdentityRole("Project Manager"));
            }

            if (!context.Roles.Any(p => p.Name == "Developer"))
            {
                roleManager.Create(new IdentityRole("Developer"));
            }

            if (!context.Roles.Any(p => p.Name == "Submitter"))
            {
                roleManager.Create(new IdentityRole("Submitter"));
            }

            ApplicationUser adminUser;

            if (!context
                .Users
                .Any(p => p.UserName == "admin@mybugtracker.com"))
            {
                adminUser = new ApplicationUser();
                adminUser.Email = "admin@mybugtracker.com";
                adminUser.Name = "Admin";
                adminUser.UserName = "admin@mybugtracker.com";

                userManager.Create(adminUser, "Password-1");
            }
            else
            {
                adminUser = context
                    .Users
                    .First(p => p.UserName == "admin@mybugtracker.com");
            }

            if (!userManager.IsInRole(adminUser.Id, "Admin"))
            {
                userManager.AddToRole(adminUser.Id, "Admin");
            }

            context.TicketPriorities.AddOrUpdate(
               new TicketPriority() { Id = 1, Name = "Low" },
               new TicketPriority() { Id = 2, Name = "Medium" },
               new TicketPriority() { Id = 3, Name = "High" }

           );



            context.TicketTypes.AddOrUpdate(
               new TicketType() { Id = 1, Name = "Bug" },
               new TicketType() { Id = 2, Name = "Feature" },
               new TicketType() { Id = 3, Name = "Database" },
               new TicketType() { Id = 4, Name = "Support" }


            );


            context.TicketStatuses.AddOrUpdate(
             new TicketStatus() { Id = 1, Name = "Open" },
             new TicketStatus() { Id = 2, Name = "Resolved" },
             new TicketStatus() { Id = 3, Name = "Rejected" }
          );
            context.SaveChanges();

            //demo admin 
            ApplicationUser DemoAdminUser = new ApplicationUser();

            if (!context.Users.Any(item => item.UserName == "demoadmin@mybugtracker.com"))
            {
                DemoAdminUser.Email = "demoadmin@mybugtracker.com";
                DemoAdminUser.Name = "DemoAdmin";
                DemoAdminUser.UserName = "demoadmin@mybugtracker.com";
                userManager.Create(DemoAdminUser, "Password-1");
            }
            else
            {
                DemoAdminUser = context.Users.FirstOrDefault(item => item.UserName == "demoadmin@mybugtracker.com");
            }
            if (!userManager.IsInRole(DemoAdminUser.Id, "Admin"))
            {
                userManager.AddToRole(DemoAdminUser.Id, "Admin");
            }

            //demo projectmanager
            ApplicationUser DemoProjectManager = new ApplicationUser();

            if (!context.Users.Any(item => item.UserName == "demoprojectmanager@mybugtracker.com"))
            {
                DemoProjectManager.Email = "demoprojectmanager@mybugtracker.com";
                DemoProjectManager.Name = "DemoProjectManager";
                DemoProjectManager.UserName = "demoprojectmanager@mybugtracker.com";
                userManager.Create(DemoProjectManager, "Password-1");
            }
            else
            {
                DemoProjectManager = context.Users.FirstOrDefault(item => item.UserName == "demoprojectmanager@mybugtracker.com");
            }
            if (!userManager.IsInRole(DemoProjectManager.Id, "Project Manager"))
            {
                userManager.AddToRole(DemoProjectManager.Id, "Project Manager");
            }

            //demo developer
            ApplicationUser DemoDeveloperUser = new ApplicationUser();

            if (!context.Users.Any(item => item.UserName == "demodeveloper@mybugtracker.com"))
            {
                DemoDeveloperUser.Email = "demodeveloper@mybugtracker.com";
                DemoDeveloperUser.Name = "DemoDeveloper";
                DemoDeveloperUser.UserName = "demodeveloper@mybugtracker.com";
                userManager.Create(DemoDeveloperUser, "Password-1");
            }
            else
            {
                DemoDeveloperUser = context.Users.FirstOrDefault(item => item.UserName == "demodeveloper@mybugtracker.com");
            }
            if (!userManager.IsInRole(DemoDeveloperUser.Id, "Developer"))
            {
                userManager.AddToRole(DemoDeveloperUser.Id, "Developer");
            }


            //demo submitter
            ApplicationUser DemoSubmitter = new ApplicationUser();

            if (!context.Users.Any(item => item.UserName == "demosubmitter@mybugtracker.com"))
            {
                DemoSubmitter.Email = "demosubmitter@mybugtracker.com";
                DemoSubmitter.Name = "DemoSubmitter";
                DemoSubmitter.UserName = "demosubmitter@mybugtracker.com";
                userManager.Create(DemoSubmitter, "Password-1");
            }
            else
            {
                DemoSubmitter = context.Users.FirstOrDefault(item => item.UserName == "demosubmitter@mybugtracker.com");
            }
            if (!userManager.IsInRole(DemoSubmitter.Id, "Submitter"))
            {
                userManager.AddToRole(DemoSubmitter.Id, "Submitter");
            }



        }
    }
}
